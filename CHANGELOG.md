# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## {2.5} - 2020-07-08
### Changed
 - Updated README.md
 - Updated installation instructions
 - Updated docker-compose.yml and traefik.toml to use new environment variables set in .env.
 - Updated docker-compose.yml with newer 64-bit images (32-bit images are commented out).

## {2.0} - 2020-06-30
### Added
 - Added Wallabag service for bookmarking/read it later functionality.
 - Added Searx for self-hosted search engine.
 - Added Syncthing for file synchronization and backup.

### Changed
 - Updated README.md.

### Removed
 - Removed Portainer.
 - Removed Nextcloud.
 - Removed Cron as it was only used by Nextcloud.
 - Removed Postgres as it was only used by Nextcloud.

## [1.5} - 2019-12-17
### Changed
 - Replaced outdated MySQL docker image with Postgres for faster performance and improved reliability.
 - Updated README and CHANGELOG

### Removed
 - Removed Pi-Hole as it was bogging down my Raspberry Pi.

## [1.0.1] - 2019-09-13
### Added
 - Updated documentation to include setting up the Pi to run off an external hard drive.
 - Added LICENSE and CONTRIBUTING sections.
 - Added .env file to explicitly state certain environment variables used within each container.

### Changed
 - Started using ItsyBitsyCloud over Itsy Bitsy Cloud as project name.
 - Updated README to include links to each project used by ItsyBitsyCloud as well as included some minor formatting tweaks.
 - Added to do section to README.
 - CHANGELOG now formatted to be based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).
 - Changed acme.json variables to be more easily readable.
 - Moved installation and configuration to Gitlab wiki from README.

### Removed
 - Setup instructions removed from README and replaced by Wiki.

## [1.0.0] - 2019-09-13
### Added
- Initial documentation via README with manual configuration procedures.
- Added CHANGELOG section.
- Initial source release.

### Changed
- No changes.

### Removed
- No changes.
